// проверка на режим тестирования
function testMode() {
    if ($jsapi.context().testContext) {
        return true;
    }
    return false;
}

// добавляем карточки (или другой reply) в response.replies
function answerPush(reply) {
    // если response.replies не существует - создаём пустой элемент массива:
    $jsapi.context().response.replies = $jsapi.context().response.replies || [];
    // добавляем reply в ответ response.replies:
    $jsapi.context().response.replies.push(reply);
}

function nothingFound(){
    $jsapi.context().response.replies = $response.replies || [];
    $jsapi.context().response.replies.push({
        type: "raw",
        messageName: "NOTHING_FOUND",
        body: {}
    });
}

// узнаём, какой персонаж у клиента, чтобы выбирать правильные реплики
function getCharacterId($request) {
    // Информация о текущем персонаже ассистента: https://developer.sberdevices.ru/docs/ru/developer_tools/amp/smartappapi_description_and_guide#объект-character
    try {
        // возможные результаты: sber, athena, joy
        return $request.rawRequest.payload.character.id;
    } catch (e) {
        if ($request.channelType === "chatwidget") {
            return "sber";
        }
        throw e.message;
    }
}

function make_agree_with_number(number, words) {
    return words[(number % 100 > 4 && number % 100 < 20) ? 2 : [2, 0, 1, 1, 1, 2][(number % 10 < 5) ? Math.abs(number) % 10 : 5]];
}

function makeFriendly(num) {

    return Math.abs(Number(num)) >= 1.0e+6

        ? (Math.abs(Number(num)) / 1.0e+6).toFixed(1) % 1 === 0
            ? Math.round(Number(num / 1.0e+6)) + " млн"
            : (Math.abs(Number(num)) / 1.0e+6).toFixed(1) + " млн"

        : Math.abs(Number(num)) >= 1.0e+3

            ? (Math.abs(Number(num)) / 1.0e+3).toFixed(0) % 1 === 0
                ? Math.round(Number(num / 1.0e+3)) + " 000"
                : (Math.abs(Number(num)) / 1.0e+3).toFixed(0) + " 000"

            : Math.abs(Number(num));

}


function get_data(text) {

    $http.config({authService: {

            service: 'platformV',
            app_key: "$credentials.platformV.app_key",
            app_secret: "$credentials.platformV.app_secret",
        }});

    var url = 'https://smapi.pv-api.sbc.space/fn_a051b883_f8fd_4a33_b839_66f609f5998f';
    var options = {
        dataType: 'json',
        headers: {
            'Content-Type': 'application/json',
        },
        body: {
            "MESSAGE_NAME": "GET_DUCKLING_RESULT",
            "data": {
                "text": String(text)
            }
        },
    };
    var response = $http.post(url, options);




    $jsapi.log(toPrettyString(response));
    return response
}

function logic(response, character){
    // проверяем что с запросом все ок
    // если нет, то уводим на экран с заглушкой без рендеринга поискового запроса
    if (response.isOk){

        var data = response.data.PAYLOAD.result


        if (response.data.STATUS){

            if (response.data.PAYLOAD.target === "search"){
                showCardList(data)
            } else {
                nothingFound()
            }
        } else {
            if (response.data.PAYLOAD.target === "search"){
                // мы успешно классифицировали результат, но не нашли автомобиль
                if (response.data.CODE === 404) {
                    if (character === 'sber') {
                        ShowCardNothingFoundSber(data)
                        answerPush({
                            "type":"buttons",
                            "buttons":[
                                {
                                    "text": "Как задать вопрос?",
                                }
                            ]
                        })
                    }
                    else if (character === 'athena') {
                        ShowCardNothingFoundAthena(data)
                        answerPush({
                            "type":"buttons",
                            "buttons":[
                                {
                                    "text": "Как задать вопрос?",
                                }
                            ]
                        })
                    }
                    else if (character === 'joy') {
                        ShowCardNothingFoundJoy(data)
                        answerPush({
                            "type":"buttons",
                            "buttons":[
                                {
                                    "text": "Как задать вопрос?",
                                }
                            ]
                        })
                    }

                }
                else {
                    if (character === 'sber') {
                        ShowCardNotClassifySber()
                        answerPush({
                            "type":"buttons",
                            "buttons":[
                                {
                                    "text": "Как задать вопрос?",
                                }
                            ]
                        })
                    }
                    else if (character === 'athena') {
                        ShowCardNotClassifyAthena()
                        answerPush({
                            "type":"buttons",
                            "buttons":[
                                {
                                    "text": "Как задать вопрос?",
                                }
                            ]
                        })
                    }
                    else if (character === 'joy') {
                        ShowCardNotClassifyJoy()
                        answerPush({
                            "type":"buttons",
                            "buttons":[
                                {
                                    "text": "Как задать вопрос?",
                                }
                            ]
                        })
                    }
                }
            } else {
                nothingFound()
            }

        }

    }
}

function startWidgets(){
    answerPush({
        "type":"buttons",
        "buttons":[
            {
                "text": "Хочу bmw 3",
            },
            {
                "text": "Skoda Octavia 2019 года в Москве",
            },
            {
                "text": "Kia Rio в Питере",
            }
        ]
    })
}

function logicQuestion(character){
    if (character === 'sber'){
        answerPush({
            "type":"text",
            "text":"Просто скажите мне один или несколько критериев: например марку и цену машины.",
            "tts":"Просто скажите мне один или несколько критериев: например марку и цену машины"
        })
    } else if (character === 'athena'){
        answerPush({
            "type":"text",
            "text":"Назовите параметры автомобиля, который хотите найти. Например, год выпуска и ценовой диапазон.",
            "tts":"Назовите параметры автомобиля, который хотите найти. Например, год выпуска и ценовой диапазон"
        })
    } else if (character === 'joy') {
        answerPush({
            "type":"text",
            "text":"Скажи, что тебе важно в машине, а я найду варианты. Например, назови марку и год выпуска.",
            "tts":"Скажи что тебе важно в машине, а я найду варианты. Например, назови марку и год выпуска"
        })
    }
    var reply = {
        "type": "raw",
        "body": {
            "emotion": null,
            "items": [{
                "card": {
                    "type": "grid_card",
                    "items": [
                        {
                            "type": "greeting_grid_item",
                            "top_text": {
                                "type": "text_cell_view",
                                "text": "Можно спросить так:",
                                "typeface": "caption",
                                "text_color": "default",
                                "max_lines": 3
                            },
                            "bottom_text": {
                                "type": "text_cell_view",
                                "text": "Сколько стоит BMW X3 2015 года?",
                                "typeface": "body3",
                                "text_color": "default",
                                "max_lines": 3,
                                "margins": {
                                    "top": "4x"
                                }
                            },
                            "paddings": {
                                "top": "6x",
                                "left": "6x",
                                "right": "6x",
                                "bottom": "6x"
                            },
                            "actions": [
                                {
                                    "text": "Сколько стоит BMW 3 серии 2015 года?"
                                }
                            ]
                        },
                        {
                            "type": "greeting_grid_item",
                            "top_text": {
                                "type": "text_cell_view",
                                "text": "Или так:",
                                "typeface": "caption",
                                "text_color": "default",
                                "max_lines": 3
                            },
                            "bottom_text": {
                                "type": "text_cell_view",
                                "text": "Покажи машины от 2010 до 2017 года в Москве",
                                "typeface": "body3",
                                "text_color": "default",
                                "max_lines": 3,
                                "margins": {
                                    "top": "4x"
                                }
                            },
                            "paddings": {
                                "top": "6x",
                                "left": "6x",
                                "right": "6x",
                                "bottom": "6x"
                            },
                            "actions": [
                                {
                                    "text": "Покажи машины от 2010 до 2017 года в Москве",
                                }
                            ]
                        }
                    ],
                    "columns": 2,
                    "item_width": "small"
                }
            }]
        },
        "messageName": "ANSWER_TO_USER"
    };

    answerPush(reply)


}