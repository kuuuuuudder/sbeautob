function showCardList(data) {

    if (data.count === 1)
        var reply = {
            "type": "raw",
            "body": {
                "pronounceText": "На экране показываю, что удалось найти. А посмотреть предложение можно в Сбер А'вто",
                "emotion": null,
                "items": [{
                    "card": {
                        "type": "list_card",
                        "cells": [
                            {
                                "type": "image_cell_view",
                                "content": {
                                    "url": "https://content.sberdevices.ru/smartmarket-smide-prod/225596/225582/nlG0njDuD6TCfkgZ.png",
                                    "hash": "48c863ccde5fb1148426ad2cc950e133",
                                    "width": "small",
                                    "aspect_ratio": 1
                                }
                            },
                            {
                                "type": "text_cell_view",
                                "content": {
                                    "text": "По запросу",
                                    "typeface": "body1",
                                    "text_color": "secondary",
                                    "max_lines": 0
                                },
                                "paddings": {
                                    "left": "8x",
                                    "top": "8x",
                                    "right": "8x"
                                }
                            },
                            {
                                "type": "text_cell_view",
                                "content": {
                                    "text": "«" + data.search_text_form + "»",
                                    "typeface": "body1",
                                    "text_color": "default",
                                    "max_lines": 0
                                },
                                "paddings": {
                                    "left": "8x",
                                    "top": "2x",
                                    "right": "8x"
                                }
                            },
                            {
                                "type": "text_cell_view",
                                "content": {
                                    "text": data.count + make_agree_with_number(data.count, [" предложение", " предложения", " предложений"]) + ' от ' + makeFriendly(data.min_price) + ' ₽',
                                    "typeface": "body1",
                                    "text_color": "default",
                                    "max_lines": 0
                                },
                                "paddings": {
                                    "left": "8x",
                                    "top": "2x",
                                    "right": "8x"
                                }
                            },
                            {
                                "type": "button_cell_view",
                                "content": {
                                    "text": "Посмотреть в СберАвто",
                                    "typeface": "button1",
                                    "style": "default",
                                    "type": "accept",
                                    "actions": [
                                        {
                                            "type": "deep_link",
                                            "text": "Посмотреть в СберАвто",
                                            "deep_link": data.url
                                        }
                                    ],
                                    "margins": {
                                        "left": "10x",
                                        "top": "6x",
                                        "right": "10x",
                                        "bottom": "6x"
                                    }
                                },
                                "paddings": {
                                    "left": "8x",
                                    "top": "8x",
                                    "right": "8x",
                                    "bottom": "8x"
                                }
                            }
                        ]
                    }
                }]
            },
            "messageName": "ANSWER_TO_USER"
        };
    else
        var reply = {
            "type": "raw",
            "body": {
                "pronounceText": "На экране показываю, какие варианты удалось найти. А посмотреть каждый из них можно в Сбер А'вто",
                "emotion": null,
                "items": [{
                    "card": {
                        "type": "list_card",
                        "cells": [
                            {
                                "type": "image_cell_view",
                                "content": {
                                    "url": "https://content.sberdevices.ru/smartmarket-smide-prod/225596/225582/nlG0njDuD6TCfkgZ.png",
                                    "hash": "48c863ccde5fb1148426ad2cc950e133",
                                    "width": "small",
                                    "aspect_ratio": 1
                                }
                            },
                            {
                                "type": "text_cell_view",
                                "content": {
                                    "text": "По запросу",
                                    "typeface": "body1",
                                    "text_color": "secondary",
                                    "max_lines": 0
                                },
                                "paddings": {
                                    "left": "8x",
                                    "top": "8x",
                                    "right": "8x"
                                }
                            },
                            {
                                "type": "text_cell_view",
                                "content": {
                                    "text": "«" + data.search_text_form + "»",
                                    "typeface": "body1",
                                    "text_color": "default",
                                    "max_lines": 0
                                },
                                "paddings": {
                                    "left": "8x",
                                    "top": "2x",
                                    "right": "8x"
                                }
                            },
                            {
                                "type": "text_cell_view",
                                "content": {
                                    "text": data.count + make_agree_with_number(data.count, [" предложение", " предложения", " предложений"]),
                                    "typeface": "body1",
                                    "text_color": "default",
                                    "max_lines": 0
                                },
                                "paddings": {
                                    "left": "8x",
                                    "top": "2x",
                                    "right": "8x"
                                }
                            },
                            {
                                "type": "text_cell_view",
                                "content": {
                                    "text": 'от ' + makeFriendly(data.min_price) + ' до ' + makeFriendly(data.max_price) + ' ₽,',
                                    "typeface": "body1",
                                    "text_color": "default",
                                    "max_lines": 0
                                },
                                "paddings": {
                                    "left": "8x",
                                    "top": "6x",
                                    "right": "8x"
                                }
                            },
                            {
                                "type": "text_cell_view",
                                "content": {
                                    "text": 'средняя цена — ' + makeFriendly(data.median) + ' ₽',
                                    "typeface": "body1",
                                    "text_color": "secondary",
                                    "max_lines": 0
                                },
                                "paddings": {
                                    "left": "8x",
                                    "top": "2x",
                                    "right": "8x"
                                }
                            },
                            {
                                "type": "button_cell_view",
                                "content": {
                                    "text": "Посмотреть в СберАвто",
                                    "typeface": "button1",
                                    "style": "default",
                                    "type": "accept",
                                    "actions": [
                                        {
                                            "type": "deep_link",
                                            "text": "Посмотреть в СберАвто",
                                            "deep_link": data.url
                                        }
                                    ],
                                    "margins": {
                                        "left": "10x",
                                        "top": "6x",
                                        "right": "10x",
                                        "bottom": "6x"
                                    }
                                },
                                "paddings": {
                                    "left": "8x",
                                    "top": "8x",
                                    "right": "8x",
                                    "bottom": "8x"
                                }
                            }
                        ]
                    }
                }]
            },
            "messageName": "ANSWER_TO_USER"
        };

    answerPush(reply);
}

function ShowCardNothingFoundSber(data) {

    var reply = {
        "type": "raw",
        "body": {
            "pronounceText": "К сожалению, по вашему запросу нет подходящих машин. Давайте поищем другой вариант. Или вы можете написать персональному менеджеру в чате Сбер А'вто, — он подберёт похожие автомобили и поможет определиться с выбором",
            "emotion": null,
            "items": [{
                "card": {
                    "type": "list_card",
                    "cells": [
                        {
                            "type": "image_cell_view",
                            "content": {
                                "url": "https://content.sberdevices.ru/smartmarket-smide-prod/225596/225582/SALzhhwohhoZcraP.png",
                                "hash": "c654cf77c39a98e6145c4001636b5e77",
                                "width": "small",
                                "aspect_ratio": 1
                            }
                        },
                        {
                            "type": "text_cell_view",
                            "content": {
                                "text": "К сожалению, по запросу «" + data.search_text_form + "» нет подходящих машин.",
                                "typeface": "body1",
                                "text_color": "default",
                                "max_lines": 0
                            },
                            "paddings": {
                                "left": "8x",
                                "top": "8x",
                                "right": "8x"
                            }
                        },
                        {
                            "type": "text_cell_view",
                            "content": {
                                "text": "Давайте поищем другой вариант. Или вы можете написать персональному менеджеру в чате СберАвто: он подберёт похожие автомобили и поможет определиться с выбором",
                                "typeface": "body1",
                                "text_color": "default",
                                "max_lines": 0
                            },
                            "paddings": {
                                "left": "8x",
                                "top": "6x",
                                "right": "8x"
                            }
                        },
                        {
                            "type": "button_cell_view",
                            "content": {
                                "text": "Перейти в СберАвто",
                                "typeface": "button1",
                                "style": "default",
                                "type": "accept",
                                "actions": [
                                    {
                                        "type": "deep_link",
                                        "text": "Перейти в СберАвто",
                                        "deep_link": "https://sberauto.com/"
                                    }
                                ],
                                "margins": {
                                    "left": "10x",
                                    "top": "6x",
                                    "right": "10x",
                                    "bottom": "6x"
                                }
                            },
                            "paddings": {
                                "left": "8x",
                                "top": "8x",
                                "right": "8x",
                                "bottom": "8x"
                            }
                        }
                    ]
                }
            }]
        },
        "messageName": "ANSWER_TO_USER"
    };

    answerPush(reply);
}

function ShowCardNothingFoundAthena(data) {

    var reply = {
        "type": "raw",
        "body": {
            "pronounceText": "Увы, я не нашла автомобиль по этому запросу. Вы можете поискать другой вариант или написать в чат Сбер А'вто. Там персональный менеджер посоветует похожие машины и ответит на ваши вопросы.",
            "emotion": null,
            "items": [{
                "card": {
                    "type": "list_card",
                    "cells": [
                        {
                            "type": "image_cell_view",
                            "content": {
                                "url": "https://content.sberdevices.ru/smartmarket-smide-prod/225596/225582/SALzhhwohhoZcraP.png",
                                "hash": "c654cf77c39a98e6145c4001636b5e77",
                                "width": "small",
                                "aspect_ratio": 1
                            }
                        },
                        {
                            "type": "text_cell_view",
                            "content": {
                                "text": "Увы, я не нашла автомобиль по запросу «" + data.search_text_form + "».",
                                "typeface": "body1",
                                "text_color": "default",
                                "max_lines": 0
                            },
                            "paddings": {
                                "left": "8x",
                                "top": "8x",
                                "right": "8x"
                            }
                        },
                        {
                            "type": "text_cell_view",
                            "content": {
                                "text": "Вы можете поискать другой вариант или написать в чат СберАвто. Там персональный менеджер посоветует похожие машины и ответит на ваши вопросы",
                                "typeface": "body1",
                                "text_color": "default",
                                "max_lines": 0
                            },
                            "paddings": {
                                "left": "8x",
                                "top": "6x",
                                "right": "8x"
                            }
                        },
                        {
                            "type": "button_cell_view",
                            "content": {
                                "text": "Перейти в СберАвто",
                                "typeface": "button1",
                                "style": "default",
                                "type": "accept",
                                "actions": [
                                    {
                                        "type": "deep_link",
                                        "text": "Перейти в СберАвто",
                                        "deep_link": "https://sberauto.com/"
                                    }
                                ],
                                "margins": {
                                    "left": "10x",
                                    "top": "6x",
                                    "right": "10x",
                                    "bottom": "6x"
                                }
                            },
                            "paddings": {
                                "left": "8x",
                                "top": "8x",
                                "right": "8x",
                                "bottom": "8x"
                            }
                        }
                    ]
                }
            }]
        },
        "messageName": "ANSWER_TO_USER"
    };

    answerPush(reply);
}

function ShowCardNothingFoundJoy(data) {

    var reply = {
        "type": "raw",
        "body": {
            "pronounceText": "Уф, всё перерыла, но по твоему запросу ничего нет. Попробуешь спросить что-то *другое? Или можешь написать персональному менеджеру в чате Сбер А'вто. Он найдёт похожие варианты и точно что-нибудь посоветует",
            "emotion": null,
            "items": [{
                "card": {
                    "type": "list_card",
                    "cells": [
                        {
                            "type": "image_cell_view",
                            "content": {
                                "url": "https://content.sberdevices.ru/smartmarket-smide-prod/225596/225582/SALzhhwohhoZcraP.png",
                                "hash": "c654cf77c39a98e6145c4001636b5e77",
                                "width": "small",
                                "aspect_ratio": 1
                            }
                        },
                        {
                            "type": "text_cell_view",
                            "content": {
                                "text": "Уф, всё перерыла, но по запросу «" + data.search_text_form + "» ничего нет.",
                                "typeface": "body1",
                                "text_color": "default",
                                "max_lines": 0
                            },
                            "paddings": {
                                "left": "8x",
                                "top": "8x",
                                "right": "8x"
                            }
                        },
                        {
                            "type": "text_cell_view",
                            "content": {
                                "text": "Попробуешь спросить что-то другое? Или можешь написать персональному менеджеру в чате СберАвто. Он найдёт похожие варианты и точно что-нибудь посоветует",
                                "typeface": "body1",
                                "text_color": "default",
                                "max_lines": 0
                            },
                            "paddings": {
                                "left": "8x",
                                "top": "6x",
                                "right": "8x"
                            }
                        },
                        {
                            "type": "button_cell_view",
                            "content": {
                                "text": "Перейти в СберАвто",
                                "typeface": "button1",
                                "style": "default",
                                "type": "accept",
                                "actions": [
                                    {
                                        "type": "deep_link",
                                        "text": "Перейти в СберАвто",
                                        "deep_link": "https://sberauto.com/"
                                    }
                                ],
                                "margins": {
                                    "left": "10x",
                                    "top": "6x",
                                    "right": "10x",
                                    "bottom": "6x"
                                }
                            },
                            "paddings": {
                                "left": "8x",
                                "top": "8x",
                                "right": "8x",
                                "bottom": "8x"
                            }
                        }
                    ]
                }
            }]
        },
        "messageName": "ANSWER_TO_USER"
    };

    answerPush(reply);
}

function ShowCardQuestion() {

    var reply = {
        "type": "raw",
        "body": {
            "emotion": null,
            "items": [
                {
                    "card": {
                        "type": "grid_card",
                        "items": [
                            {
                                "type": "greeting_grid_item",
                                "top_text": {
                                    "type": "text_cell_view",
                                    "text": "Сколько стоит BMW X3 2015 года",
                                    "typeface": "caption",
                                    "text_color": "default"
                                },
                                "paddings": {
                                    "top": "6x",
                                    "left": "6x",
                                    "right": "6x",
                                    "bottom": "6x"
                                },
                                "actions": [
                                    {
                                        "text": "Сколько стоит BMW X3 2015 года"
                                    }
                                ]
                            },
                            {
                                "type": "greeting_grid_item",
                                "top_text": {
                                    "type": "text_cell_view",
                                    "text": "Покажи машины 2010-2017 года выпуска до 3 млн ₽",
                                    "typeface": "caption",
                                    "text_color": "default"
                                },
                                "paddings": {
                                    "top": "6x",
                                    "left": "6x",
                                    "right": "6x",
                                    "bottom": "6x"
                                },
                                "actions": [
                                    {
                                        "text": "Покажи машины 2010-2017 года выпуска до 3 млн ₽"
                                    }
                                ]
                            }
                        ],
                        "columns": 2,
                        "item_width": "small"
                    }
                }]
        },
        "messageName": "ANSWER_TO_USER"
    };

    answerPush(reply);
}

// карточки для случая где мы не можем классифицировать текст

function ShowCardNotClassifyAthena() {

    var reply = {
        "type": "raw",
        "body": {
            "pronounceText": "Увы, я не нашла автомобиль по этому запросу. Вы можете поискать другой вариант или написать в чат Сбер А'вто. Там персональный менеджер посоветует похожие машины и ответит на ваши вопросы",
            "emotion": null,
            "items": [{
                "card": {
                    "type": "list_card",
                    "cells": [
                        {
                            "type": "image_cell_view",
                            "content": {
                                "url": "https://content.sberdevices.ru/smartmarket-smide-prod/225596/225582/SALzhhwohhoZcraP.png",
                                "hash": "c654cf77c39a98e6145c4001636b5e77",
                                "width": "small",
                                "aspect_ratio": 1
                            }
                        },
                        {
                            "type": "text_cell_view",
                            "content": {
                                "text": "Вы можете поискать другой вариант или написать в чат СберАвто. Там персональный менеджер посоветует похожие машины и ответит на ваши вопросы",
                                "typeface": "body1",
                                "text_color": "default",
                                "max_lines": 0
                            },
                            "paddings": {
                                "left": "8x",
                                "top": "8x",
                                "right": "8x"
                            }
                        },
                        {
                            "type": "button_cell_view",
                            "content": {
                                "text": "Перейти в СберАвто",
                                "typeface": "button1",
                                "style": "default",
                                "type": "accept",
                                "actions": [
                                    {
                                        "type": "deep_link",
                                        "text": "Перейти в СберАвто",
                                        "deep_link": "https://sberauto.com/"
                                    }
                                ],
                                "margins": {
                                    "left": "10x",
                                    "top": "6x",
                                    "right": "10x",
                                    "bottom": "6x"
                                }
                            },
                            "paddings": {
                                "left": "8x",
                                "top": "8x",
                                "right": "8x",
                                "bottom": "8x"
                            }
                        }
                    ]
                }
            }]
        },
        "messageName": "ANSWER_TO_USER"
    };

    answerPush(reply);
}

function ShowCardNotClassifySber() {

    var reply = {
        "type": "raw",
        "body": {
            "pronounceText": "К сожалению, по вашему запросу нет подходящих машин. Давайте поищем другой вариант. Или вы можете написать персональному менеджеру в чате Сбер А'вто, — он подберёт похожие автомобили и поможет определиться с выбором",
            "emotion": null,
            "items": [{
                "card": {
                    "type": "list_card",
                    "cells": [
                        {
                            "type": "image_cell_view",
                            "content": {
                                "url": "https://content.sberdevices.ru/smartmarket-smide-prod/225596/225582/SALzhhwohhoZcraP.png",
                                "hash": "c654cf77c39a98e6145c4001636b5e77",
                                "width": "small",
                                "aspect_ratio": 1
                            }
                        },
                        {
                            "type": "text_cell_view",
                            "content": {
                                "text": "Давайте поищем другой вариант. Или вы можете написать персональному менеджеру в чате СберАвто: он подберёт похожие автомобили и поможет определиться с выбором",
                                "typeface": "body1",
                                "text_color": "default",
                                "max_lines": 0
                            },
                            "paddings": {
                                "left": "8x",
                                "top": "8x",
                                "right": "8x"
                            }
                        },
                        {
                            "type": "button_cell_view",
                            "content": {
                                "text": "Перейти в СберАвто",
                                "typeface": "button1",
                                "style": "default",
                                "type": "accept",
                                "actions": [
                                    {
                                        "type": "deep_link",
                                        "text": "Перейти в СберАвто",
                                        "deep_link": "https://sberauto.com/"
                                    }
                                ],
                                "margins": {
                                    "left": "10x",
                                    "top": "6x",
                                    "right": "10x",
                                    "bottom": "6x"
                                }
                            },
                            "paddings": {
                                "left": "8x",
                                "top": "8x",
                                "right": "8x",
                                "bottom": "8x"
                            }
                        }
                    ]
                }
            }]
        },
        "messageName": "ANSWER_TO_USER"
    };

    answerPush(reply);
}

function ShowCardNotClassifyJoy() {

    var reply = {
        "type": "raw",
        "body": {
            "pronounceText": "Уф, всё перерыла, но по твоему запросу ничего нет. Попробуешь спросить что-то *другое? Или можешь написать персональному менеджеру в чате Сбер А'вто. Он найдёт похожие варианты и точно что-нибудь посоветует.",
            "emotion": null,
            "items": [{
                "card": {
                    "type": "list_card",
                    "cells": [
                        {
                            "type": "image_cell_view",
                            "content": {
                                "url": "https://content.sberdevices.ru/smartmarket-smide-prod/225596/225582/SALzhhwohhoZcraP.png",
                                "hash": "c654cf77c39a98e6145c4001636b5e77",
                                "width": "small",
                                "aspect_ratio": 1
                            }
                        },
                        {
                            "type": "text_cell_view",
                            "content": {
                                "text": "Попробуешь спросить что-то другое? Или можешь написать персональному менеджеру в чате СберАвто. Он найдёт похожие варианты и точно что-нибудь посоветует",
                                "typeface": "body1",
                                "text_color": "default",
                                "max_lines": 0
                            },
                            "paddings": {
                                "left": "8x",
                                "top": "8x",
                                "right": "8x"
                            }
                        },
                        {
                            "type": "button_cell_view",
                            "content": {
                                "text": "Перейти в СберАвто",
                                "typeface": "button1",
                                "style": "default",
                                "type": "accept",
                                "actions": [
                                    {
                                        "type": "deep_link",
                                        "text": "Перейти в СберАвто",
                                        "deep_link": "https://sberauto.com/"
                                    }
                                ],
                                "margins": {
                                    "left": "10x",
                                    "top": "6x",
                                    "right": "10x",
                                    "bottom": "6x"
                                }
                            },
                            "paddings": {
                                "left": "8x",
                                "top": "8x",
                                "right": "8x",
                                "bottom": "8x"
                            }
                        }
                    ]
                }
            }]
        },
        "messageName": "ANSWER_TO_USER"
    };

    answerPush(reply);
}
