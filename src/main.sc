require: scripts/scripts.js
require: scripts/cards.js
require: answers.yaml
  var = $Answers
init:
    bind("onAnyError", function($context) {
        // выбор формулировки для ошибки, в зависимости от режима тестирования:
        if (!testMode()) {
            log("ERROR! " + $context.exception.message);
            $reactions.answer($Answers.Error);
        } else {
            throw "ERROR! " + $context.exception.message;
        }
    });

theme: /

    state: Start
        q!: $regex</start>
        script:
            // Начало новой сессии: https://developer.sberdevices.ru/docs/ru/developer_tools/ide/JS_API/built_in_services/jsapi/startSession
            if ($parseTree.value === "start") { $jsapi.startSession() };
            // Переменные JS API – $session: https://developer.sberdevices.ru/docs/ru/developer_tools/ide/JS_API/variables/session
            $session.character = getCharacterId($request);
            // реплика из answers.yaml, в зависимости от персонажа:
            $reactions.answer($Answers["Start"][$session.character]);
            startWidgets()

    state: Exit
        q!: (пока|до свидания |bye |завершить |выход |стоп | хватит) *
        script:
            $jsapi.stopSession();
        random:
            a: Всего доброго.
            a: До свидания!

    state: Question
        q!: (Как задать вопрос)
        script:
            var character = getCharacterId($request)
            logicQuestion(character)


    state: Car
        q!: *
        script:
            $jsapi.log(toPrettyString($request));
            $session.character = getCharacterId($request);
            var character = getCharacterId($request);

            var human_normalized_text = $request.rawRequest.payload.message.human_normalized_text;

            var response_from_api = get_data(human_normalized_text)

            logic(response_from_api, character)
